/*
 * For work developed by the HSQL Development Group:
 *
 * Copyright (c) 2001-2011, The HSQL Development Group
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * Neither the name of the HSQL Development Group nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL HSQL DEVELOPMENT GROUP, HSQLDB.ORG,
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 *
 * For work originally developed by the Hypersonic SQL Group:
 *
 * Copyright (c) 1995-2000, The Hypersonic SQL Group.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * Neither the name of the Hypersonic SQL Group nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE HYPERSONIC SQL GROUP,
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software consists of voluntary contributions made by many individuals
 * on behalf of the Hypersonic SQL Group.
 */

import java.io.*;
import java.awt.*;
import java.util.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Extract a directory tree and store in an HSQLDB database.
 *
 * @author Thomas Mueller (Hypersonic SQL Group)
 * @version 1.7.0
 * @since Hypersonic SQL
 */
class FindFileMain {
	private static wordCounter wordcounter = new wordCounter();
	/**
	 * Extracts a directory tree and stores it ina HSQLDB database.<br>
	 * Usage:<p>
	 * <pre>
	 * java org.hsqldb.sample.FindFile -init .
	 * Re-create database from directory '.'
	 * java org.hsqldb.sample.FindFile name
	 * Find files like 'name'
	 * </pre>
	 *
	 * @param arg
	 */
	public static void main(String[] arg) {

		// Exceptions may occur
		try {

			// Load the HSQL Database Engine JDBC driver
			Class.forName("org.hsqldb.jdbc.JDBCDriver");

			// Connect to the database
			// It will be create automatically if it does not yet exist
			// 'testfiles' in the URL is the name of the database
			// "SA" is the user name and "" is the (empty) password
			Connection conn =
				DriverManager.getConnection("jdbc:hsqldb:testfiles", "SA",
						"");

			// Check the command line parameters

			if ((arg.length == 1) && arg[0].equalsIgnoreCase("-init")) {

				// Command line parameters: -init pathname
				// Init the database and fill all file names in
				fillDB(conn);
			}
			
			else if ((arg.length == 3) && arg[0].equalsIgnoreCase("-update")) {

				update(conn, arg[1], arg[2]);
			}
			
			else if ((arg.length == 2) && arg[0].equalsIgnoreCase("-delete")) {

				// Command line parameters: -init pathname
				// Init the database and fill all file names in
				delete(conn, arg[1]);
			}
			else if (arg.length == 2 && arg[0].equalsIgnoreCase("-select")) {

				// One parameter:
				// Find and print the list of files that are like this
				findWord(conn, arg[1]);

			}else if (arg.length == 2 && arg[0].equalsIgnoreCase("-selectlike")){
				findWordLike(conn, arg[1]);
			}else{

				// Display the usage info
				System.out.println("Usage:");
				System.out.println("java FindFileMain -init");
				System.out.println("  Insert words and occurances from file into db");
				System.out.println("java FindFileMain name");
				System.out.println("  Find occurances of 'name'");
				System.out.println("java FindFileMain like 'name'");
				System.out.println("  Occurances of words containing 'name'");
			}

			// Finally, close the connection
			conn.close();
			System.exit(0);

		} catch (Exception e) {

			// Print out the error message
			System.out.println(e);
			e.printStackTrace();
		}


	}

	static void fillDB(Connection conn) throws SQLException, FileNotFoundException, InterruptedException {

		System.out.println("Select a file to populate the Database");

		// Create a statement object
		Statement stat = conn.createStatement();

		// Try to drop the table
		try {
			stat.executeUpdate("DROP TABLE Files");
		} catch (SQLException e) {    // Ignore Exception, because the table may not yet exist
		}

		// For compatibility to other database, use varchar(255)
		// In HSQL Database Engine, length is unlimited, like Java Strings
		// String [] execute = {"CREATE TABLE Files", "(Word varchar(255),Value INT);"};
		stat.execute("CREATE TABLE Files"
				+ "(Word varchar(255),Value INT)");

		// Close the Statement object, it is no longer used
		stat.close();

		// Use a PreparedStatement because Path and Name could contain '
		PreparedStatement prep =
			conn.prepareCall("INSERT INTO Files (Word,Value) VALUES (?,?)");


		//populating the database
		HashMap <String, Integer> map = new HashMap<String, Integer>();
		try{
			String filename = fileDialog(new Frame(), "Open", ".\\", "*.txt");
			String [] wordsInFile = wordcounter.readFileStringArray(filename);
			map = wordcounter.countWords(wordsInFile);

			populate(map,prep);
		}catch(Exception fnfe){
			System.out.println("File not found or dialog closed");
			//because wait to close		
			String progress = "Quitting...";
			for(int i=progress.indexOf("."); i< progress.length(); i++){
				System.out.print(progress.substring(0,i) + "\r");
				Thread.sleep(1000);
			}
			System.exit(-1);
		}

		// Close the PreparedStatement
		prep.close();
		System.out.println("Finished");
	}

	static void populate(HashMap<String, Integer> map, PreparedStatement prep) throws SQLException{
		for (Map.Entry<String, Integer> entry : map.entrySet()) {
			prep.clearParameters();
			String key = entry.getKey().toString();
			int value = entry.getValue().intValue();
			prep.setString(1, key);
			prep.setInt(2, value);
			//System.out.println("key: " + key + "\tvalue: " + value + "\n");
			prep.execute();
		}


	}

	static void findWord(Connection conn, String keyword) throws SQLException{
		System.out.print("Occurances of \"" + keyword + "\": ");
		keyword = keyword.toUpperCase();
		Statement stat = conn.createStatement();

		ResultSet result = stat.executeQuery("SELECT Value FROM Files WHERE UCASE(Word) = '" + keyword + "'");
		//result = stat.executeQuery("SELECT Value FROM Files");
		// Moves to the next record until no more records
		// while (result.next()) {

		// Print the first column of the result
		// could use also getString("Path")
		//
		//
		//result.beforeFirst();
		//System.out.println(result.getMetaData());
		//if(!result.next()){
		//	System.out.println("0");
		//}
		if(result.next()){
			System.out.println(result.getInt("Value"));
			//System.out.println("asdf");
			//}

			// Close the ResultSet - not really necessary, but recommended

		}else{
			System.out.println("0");
		}
		result.close();

	}

	static void findWordLike(Connection conn, String keyword) throws SQLException{
		System.out.println("Occurances of words containg '" + keyword + "':");
		keyword = keyword.toUpperCase();
		Statement stat = conn.createStatement();

		ResultSet result = stat.executeQuery("SELECT * FROM Files WHERE UCASE(Word) LIKE '%"
											+ keyword + "%'");
		if(!result.next()){

			System.out.println("No words containing '" + keyword + "'");

		}else{
			do{

				System.out.printf("%-15s" + result.getInt(2) + "\n", result.getString(1) + ":");

			}while(result.next());
		}

		result.close();
	}
	
	static void update(Connection conn, String word, String value) throws SQLException{
		int newval = Integer.parseInt(value);
		Statement stat = conn.createStatement();
		stat.executeQuery("UPDATE Files SET Value =" + newval + "WHERE Word = '" + word + "'");
		System.out.println("New value for: " + word);
		findWord(conn, word);
	
	
	}
	
	static void delete(Connection conn, String word) throws SQLException{
		Statement stat = conn.createStatement();
		stat.executeQuery("DELETE FROM Files WHERE Word='" + word + "'");
		System.out.println("Removed " + word);
		findWord(conn, word);
	
	
	}


	@SuppressWarnings("deprecation")
	public static String fileDialog
	(Frame f, String title, String defDir, String fileType) throws FileNotFoundException {
		FileDialog fd = new FileDialog(f, title, FileDialog.LOAD);
		fd.setFile(fileType);
		fd.setDirectory(defDir);
		fd.setLocation(50, 50);
		fd.show();
		//BufferedReader bf = new BufferedReader(new FileReader(fd.getFile()));
		return fd.getFile(); //grabs the file
	}

}

//SELECT * FROM INFORMATION_SCHEMA.SYSTEM_TABLES
//SELECT * FROM INFORMATION_SCHEMA.SYSTEM_COLUMNS
