import java.io.*;
import java.util.*;
import java.sql.*;
import java.awt.*;



class wordCounter{
	private static double fileLength = 1; //divide by 0?
	private static int count = 0; //for the recursive method
	private static HashMap<String, Integer> returnedMap = new HashMap<String, Integer>(); //for the recursive method
	
	public static void main(String [] cheese){
		Map<String, Integer> wordCount = new HashMap<String, Integer>();
		try{
			String hold = fileDialog(new Frame(), "Open", ".\\", "*.*");
			String [] file = readFileStringArray(hold);
			System.out.println(hold);
			wordCount = countWords(file);
			
			for (Map.Entry<String, Integer> entry : wordCount.entrySet()) {
              			String key = entry.getKey().toString();
              			Integer value = entry.getValue();
              			System.out.printf("Word: %-15s\t Occurances: %-2d\n", key, value );
            		}
            		
			System.exit(0);
		}catch(Exception fnfe) {fnfe.printStackTrace(); System.exit(-1);}
		
	
	}
	
	
	public static String[] readFileStringArray(String filename) throws FileNotFoundException{
		File f = new File(filename);
		fileLength = f.length();
		Scanner input = new Scanner(new File(filename));
		StringBuilder sb = new StringBuilder();
		String [] output = null;
		String hold = null;
		for(int i = 0; input.hasNext(); i++){
			double j = i;
			hold = input.next().replaceAll("[^a-zA-Z]+", "").toLowerCase();
			//hold = input.nextLine().toLowerCase();
			//progress bar???
			
			System.out.printf("\rProgress: %.1f", (100 * (j/fileLength)+78));
			sb.append(hold+" ");
		}
		System.out.printf("\rProgress: %.1f", 100.0);
		return output = sb.toString().split(" ");
	}
	
	
	public static HashMap<String, Integer> countWords(String [] words){
		
		//Map<String, Integer> returnedMap = new HashMap<String, Integer>();
		ArrayList <String> builder = new ArrayList<String>(Arrays.asList(words));
		String hold = "";
		if (words.length == 0) return returnedMap;
		for(String word : builder){
			hold = word;
			count = Collections.frequency(builder, word);
			returnedMap.put(word, count);
			if(count != 0) break;
				
		
		}
		builder.removeAll(Collections.singleton(hold)); //notice concureny modification for iteration otherwise this would be easier
		Object objectArray[] = builder.toArray();
		String [] holder =  builder.toArray(new String[builder.size()]); //Arrays.asList(objectArray).toArray(new String[objectArray.length]); // Arrays.copyOf(objectArray, objectArray.length, String[].class);
		
		countWords(holder); //turning obj array into string array
		return returnedMap;
	
	}
	
	@SuppressWarnings("deprecation")
    public static String fileDialog
    (Frame f, String title, String defDir, String fileType) throws FileNotFoundException {
    	FileDialog fd = new FileDialog(f, title, FileDialog.LOAD);
    	fd.setFile(fileType);
    	fd.setDirectory(defDir);
    	fd.setLocation(50, 50);
   	 fd.show();
    //BufferedReader bf = new BufferedReader(new FileReader(fd.getFile()));
    	return fd.getFile(); //grabs the file
    }
}






